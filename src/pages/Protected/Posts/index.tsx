import React from 'react';
import { useGetPostsQuery, useAddPostMutation } from 'redux/reducers/posts';
import FallBackLoader from 'components/FallBackLoader';
import { Table as PostTable } from 'components/CustomDataTable';
import CssBaseline from '@material-ui/core/CssBaseline';
import { Button } from '@material-ui/core';
import AddIcon from '@material-ui/icons/Add';
import useToggle from 'hooks/useToggle';
import PostFormsWithFormiksWithModal from './postForm';
const Posts = () => {
  const { data, error, isLoading } = useGetPostsQuery();
  const [addPosts, { isLoading: isPostAdding }] = useAddPostMutation();
  const [postFormModal, togglePostFormModal] = useToggle(false);

  //   const [formData, setformData] = React.useState(initialState);

  const columns = React.useMemo(
    () => [
      {
        Header: 'Author',
        accessor: 'author',
      },
      {
        Header: 'Title',
        accessor: 'title',
      },
    ],
    []
  );

  const handleOnSubmitClicked = (data) => {
    addPosts(data)
      .unwrap()
      .then((payload) => {
        togglePostFormModal();
      });
  };

  if (isLoading) {
    return <FallBackLoader />;
  }

  return (
    <>
      <CssBaseline />
      <Button variant="contained" color="primary" startIcon={<AddIcon />} onClick={() => togglePostFormModal()}>
        Create New Posts
      </Button>
      <PostTable data={data || []} columns={columns} pagination={true} filter={true} />
      <PostFormsWithFormiksWithModal
        postsAdditionFormModal={postFormModal}
        togglepostAdditionFormModal={togglePostFormModal}
        modalTitle={'Add Posts'}
        onSubmit={handleOnSubmitClicked}
      />
    </>
  );
};

export default Posts;
