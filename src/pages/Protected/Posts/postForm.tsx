import React from 'react';
import FilledInput from '@material-ui/core/FilledInput';
import FormControl from '@material-ui/core/FormControl';
import InputLabel from '@material-ui/core/InputLabel';
import { useFormik } from 'formik';
import { FormikProps } from 'formik';
import { Grid, Typography, DialogContent, DialogActions, Button } from '@material-ui/core';
import FormikValidationError from 'components/FormikErrors';
import ComponentModal from 'components/Modal';
interface postsAddForm {
  title: string;
  author: string;
  descriptions: string;
}

interface IPostFormss extends FormikProps<postsAddForm> {}

export function PostForms(props: IPostFormss) {
  const onInputChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    props.setFieldValue(event.target.name, event.target.value);
  };

  return (
    <div>
      <Grid container direction="row" justifyContent="flex-start" alignItems="flex-start">
        <Grid xs={12} className="mt-3">
          <FormControl variant="filled" style={{ width: '100%' }}>
            <InputLabel htmlFor="component-filled">posts Name</InputLabel>
            <FilledInput
              id="component-filled"
              value={props.values.title}
              onChange={onInputChange}
              aria-describedby="component-error-text"
              name="title"
            />

            <FormikValidationError name="title" errors={props.errors} touched={props.touched} />
          </FormControl>
        </Grid>
        <Grid xs={12} className="mt-3">
          <FormControl variant="filled" style={{ width: '100%' }}>
            <InputLabel htmlFor="component-filled">Author Name</InputLabel>
            <FilledInput
              id="component-filled"
              value={props.values.author}
              onChange={onInputChange}
              aria-describedby="component-error-text"
              name="author"
            />

            <FormikValidationError name="author" errors={props.errors} touched={props.touched} />
          </FormControl>
        </Grid>

        <Grid xs={12} className="mt-3">
          <FormControl variant="filled" style={{ width: '100%' }}>
            <InputLabel htmlFor="component-filled">Descriptions</InputLabel>
            <FilledInput
              id="component-filled"
              value={props.values.descriptions}
              onChange={onInputChange}
              aria-describedby="component-error-text"
              multiline
              rows={6}
              name="descriptions"
            />
            <FormikValidationError name="descriptions" errors={props.errors} touched={props.touched} />
            {/* <FormHelperText id="component-error-text">Error</FormHelperText> */}
          </FormControl>
        </Grid>
      </Grid>
    </div>
  );
}

interface IPostFormsWithFormiks {
  formData?: postsAddForm;
  onSubmit: (data: postsAddForm) => void;
  toggleModal: () => void;
}
export const PostFormsWithFormiks = (props: IPostFormsWithFormiks) => {
  const { formData, onSubmit, toggleModal } = props;
  const initialValues: postsAddForm = {
    title: '',
    author: '',
    descriptions: '',
  };
  const formik = useFormik({
    initialValues: formData ? formData : initialValues,
    onSubmit,
  });

  return (
    <form onSubmit={formik.handleSubmit}>
      <DialogContent>
        <PostForms {...formik} />
      </DialogContent>
      <DialogActions>
        <Button variant="outlined" onClick={toggleModal} color="secondary" style={{ textTransform: 'none' }}>
          Close
        </Button>
        <Button variant="contained" color="primary" type={'submit'} style={{ textTransform: 'none' }}>
          Submit
        </Button>
      </DialogActions>
    </form>
  );
};

interface IPostFormsWithFormiksWithModalProps {
  postsAdditionFormModal: boolean;
  togglepostAdditionFormModal: () => void;
  modalTitle: string;
  formData?: postsAddForm;
  onSubmit: (data: postsAddForm) => void;
}
const PostFormsWithFormiksWithModal = (props: IPostFormsWithFormiksWithModalProps) => {
  const { postsAdditionFormModal, togglepostAdditionFormModal, modalTitle, formData, onSubmit } = props;

  return (
    <ComponentModal
      isModalOpen={postsAdditionFormModal}
      toggleModal={togglepostAdditionFormModal}
      renderTitle={() => {
        return <Typography variant="body1">{modalTitle}</Typography>;
      }}
      submitLabel={'Save'}
      buttonType={'submit'}
    >
      <PostFormsWithFormiks formData={formData} onSubmit={onSubmit} toggleModal={togglepostAdditionFormModal} />
    </ComponentModal>
  );
};
export default PostFormsWithFormiksWithModal;
