import React from 'react';
import { IRoutes, routes } from 'routes/routesLists';
import MiniDrawer from './Dasboard';

interface IProtected {
  childrenRoutes: IRoutes[];
}

const Protected = (props: IProtected) => {
  const { childrenRoutes } = props;

  return <MiniDrawer routes={childrenRoutes} />;
};

export default Protected;
