import i18n from 'i18next';
import { initReactI18next } from 'react-i18next';

export enum language {
  EN = 'en',
  NE = 'ne',
}

export const i18nLanguages: Array<language> = [language.EN, language.NE];

// Translation resources
const resources = {
  en: {},
  ne: {},
};

i18n
  .use(initReactI18next) // passes i18n down to react-i18next
  .init({
    lng: language.EN,
    fallbackLng: language.EN,
    whitelist: i18nLanguages,
    resources,
    ns: ['common'],
    defaultNS: 'common',
    fallbackNS: 'common',
    keySeparator: '.', // we use keys in form {t('messages.welcome')}
    interpolation: {
      escapeValue: false, // react already safes from xss
    },
  });

export default i18n;
