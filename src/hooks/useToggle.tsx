import { useState } from 'react';

export default function useToggle(defaultValue: boolean): [boolean, (value?: boolean) => void] {
  const [value, setvalue] = useState(defaultValue);
  function toggleValue(value?: boolean) {
    setvalue((currentValue) => (typeof value === 'boolean' ? value : !currentValue));
  }

  return [value, toggleValue];
}
