import React from 'react';
import './App.css';
import PrivateRoutes from 'routes';
import { routes } from 'routes/routesLists';

function App() {
  return <PrivateRoutes routes={routes} />;
}

export default App;
