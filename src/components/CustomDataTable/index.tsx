import React, { useEffect } from 'react';
import MaUTable from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import {
  useTable,
  useFlexLayout,
  useGlobalFilter,
  usePagination,
  useSortBy,
  ColumnInstance,
  Column,
  Row,
  useExpanded,
} from 'react-table';
import { TableFooter } from '@material-ui/core';
import TablePagination from '@material-ui/core/TablePagination';
import TextField from '@material-ui/core/TextField';
interface ITableProps {
  columns: Column;
  data: any;
  pagination?: boolean;
  totalNumberOfElements?: number;
  serverPagination?: boolean;
  onChangeRowPerPage?: (rowCount: number) => void;
  onPageChange?: (pageCount: number) => void;
  rowsPerPage?: number;
  filter?: boolean;
  filterText?: string;
}

export function Table(props: ITableProps) {
  const {
    columns,
    data,
    pagination,
    totalNumberOfElements,
    onPageChange,
    onChangeRowPerPage,
    rowsPerPage,
    filterText,
    filter,
  } = props;

  // Use the state and functions returned from useTable to build your UI
  const tableLayout = useTable(
    {
      columns,
      data,
    },
    useGlobalFilter,
    useSortBy,
    useExpanded,
    usePagination
  );

  const {
    getTableProps,
    getTableBodyProps,
    headerGroups,
    rows,
    prepareRow,
    page,
    canPreviousPage,
    canNextPage,
    gotoPage,
    nextPage,
    previousPage,
    setPageSize,
    setGlobalFilter,
    allColumns,
    footerGroups,

    state: { pageIndex, pageSize, globalFilter },
  } = tableLayout;

  useEffect(() => {
    // For Filter of all columns
    if (filter) {
      setGlobalFilter(filterText);
    } else {
      setGlobalFilter('');
    }
  }, [filterText, filter, setGlobalFilter]);

  const handleChangeRowsPerPage = (event: React.ChangeEvent<HTMLInputElement>) => {
    if (onChangeRowPerPage) {
      onChangeRowPerPage(parseInt(event.target.value, 10));
    }
    setPageSize(parseInt(event.target.value, 10));

    // setRowsPerPage(parseInt(event.target.value, 10));
  };

  const handleChangePage = (event: unknown, newPage: number) => {
    if (onPageChange) {
      onPageChange(newPage);
    }

    gotoPage(newPage);
  };
  // Render the UI for your table

  const tableData = pagination ? page : rows;

  return (
    <>
      {filter && (
        <TextField
          id="filled-search"
          label="Search field"
          type="search"
          variant="filled"
          value={globalFilter}
          onChange={(e) => setGlobalFilter(e.target.value)}
        />
      )}
      <MaUTable {...getTableProps()}>
        <TableHead>
          {headerGroups.map((headerGroup) => (
            <TableRow {...headerGroup.getHeaderGroupProps()}>
              {headerGroup.headers.map((column) => (
                <TableCell {...column.getHeaderProps()}>{column.render('Header')}</TableCell>
              ))}
            </TableRow>
          ))}
        </TableHead>
        <TableBody>
          {tableData.map((row, i) => {
            prepareRow(row);

            return (
              <TableRow {...row.getRowProps()}>
                {row.cells.map((cell) => {
                  return <TableCell {...cell.getCellProps()}>{cell.render('Cell')}</TableCell>;
                })}
              </TableRow>
            );
          })}
        </TableBody>
        {pagination && (
          <TableFooter>
            <TableRow>
              <TablePagination
                rowsPerPageOptions={[5, 10, 25, { label: 'All', value: -1 }]}
                colSpan={3}
                count={totalNumberOfElements || rows.length}
                rowsPerPage={rowsPerPage || pageSize}
                page={pageIndex}
                SelectProps={{
                  inputProps: { 'aria-label': 'rows per page' },
                  native: true,
                }}
                onPageChange={handleChangePage}
                onRowsPerPageChange={handleChangeRowsPerPage}
                // ActionsComponent={TablePaginationActions}
              />
            </TableRow>
          </TableFooter>
        )}
      </MaUTable>
    </>
  );
}

export default Table;
