import { configureStore, ThunkAction, Action } from '@reduxjs/toolkit';
import reducer, { middleware } from 'redux/reducers';

export const store = configureStore({
  reducer,
  middleware,
});

export type AppDispatch = typeof store.dispatch;
export type RootState = ReturnType<typeof store.getState>;
export type AppThunk<ReturnType = void> = ThunkAction<ReturnType, RootState, unknown, Action<string>>;
