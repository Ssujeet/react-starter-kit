import { combineReducers } from 'redux';
import counterReducer from 'redux/reducers/counter/counterSlice';
import { postApi } from 'redux/reducers/posts';
const reducers = combineReducers({
  counter: counterReducer,
  [postApi.reducerPath]: postApi.reducer,
});

export const middleware = (getDefaultMiddleware) => getDefaultMiddleware().concat(postApi.middleware);

export default reducers;
