import { fetchBaseQuery } from '@reduxjs/toolkit/query';

export const baseQuery = (authentication: boolean) =>
  fetchBaseQuery({
    baseUrl: 'http://localhost:3000/',
    prepareHeaders: (headers, { getState }) => {
      const token = '';

      // If we have a token set in state, let's assume that we should be passing it.
      if (token && authentication) {
        headers.set('authorization', `Bearer ${token}`);
      }
      // headers.set("Content-Type", contentType);

      return headers;
    },
  });

export function convertJsonToFormData(requestData: { [key: string]: any }) {
  let formData = new FormData();
  for (let data in requestData) {
    if (requestData[data] instanceof Array) {
      requestData[data].forEach((dataEl: any, index: number) => {
        if (dataEl instanceof Object && !(dataEl instanceof File)) {
          Object.keys(dataEl).forEach((elKey) => formData.append(`${data}[${index}].${elKey}`, dataEl[elKey]));
        } else if (dataEl instanceof File) {
          formData.append(`${data}[${index}]`, dataEl);
        } else if (typeof dataEl === 'number' || typeof dataEl === 'string') {
          formData.append(`${data}[${index}]`, dataEl.toString());
        }
      });
    } else if (requestData[data] instanceof Object && !(requestData[data] instanceof File)) {
      Object.entries(requestData[data]).forEach(([key, value]: [string, any]) =>
        formData.append(`${data}.${key}`, value)
      );
    } else {
      formData.append(data, requestData[data]);
    }
  }

  return formData;
}
