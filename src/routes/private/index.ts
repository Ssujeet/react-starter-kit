/* eslint-disable react/display-name */
import React, { ReactNode } from 'react';
import { IRoutes } from 'routes/routesLists';

const Protected = React.lazy(() => import('../../../src/pages/Protected'));
const Home = React.lazy(() => import('../../../src/pages/Protected/Home'));
const Settings = React.lazy(() => import('../../../src/pages/Protected/Settings'));
const Posts = React.lazy(() => import('../../../src/pages/Protected/Posts'));

export const privateRoutes: IRoutes[] = [
  {
    path: '/',
    title: 'DashBoard',
    component: Protected,
    exact: true,
    childrenRoutes: [
      { path: '/', title: 'Home', component: Home, exact: true },
      { path: '/settings', title: 'Settings', component: Settings, exact: true },
      { path: '/posts', title: 'Posts', component: Posts, exact: true },
    ],
  },
];
