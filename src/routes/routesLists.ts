/* eslint-disable react/display-name */
import React, { ReactNode } from 'react';
import { publicRoutes } from 'routes/public';
import { privateRoutes } from 'routes/private';

export interface IRoutes {
  path: string;
  title: string;
  component: React.ComponentType<any>;
  exact?: boolean;
  notShowToNavBar?: boolean;
  childrenRoutes?: IRoutes[];
  isPublic?: boolean;
}

export const routes: IRoutes[] = [...publicRoutes, ...privateRoutes];
