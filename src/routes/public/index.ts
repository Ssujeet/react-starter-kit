/* eslint-disable react/display-name */
import React, { ReactNode } from 'react';
import { IRoutes } from 'routes/routesLists';
const SignIn = React.lazy(() => import('../../../src/pages/Public/SigIn'));
const SignUp = React.lazy(() => import('../../../src/pages/Public/SignUp'));

export const publicRoutes: IRoutes[] = [
  {
    path: '/sign',
    title: 'Sign',
    component: SignIn,
    exact: true,
    isPublic: true
  },
  {
    path: '/signup',
    title: 'signup',
    component: SignUp,
    exact: true,
    isPublic: true
  },
];
